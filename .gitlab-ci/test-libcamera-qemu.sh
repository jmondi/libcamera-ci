#!/bin/bash

# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: © 2023 Laurent Pinchart <laurent.pinchart@ideasonboard.com>
#
# Run the libcamera unit tests

set -e

source "$(dirname "$0")/lib.sh"

PKGS_LIBCAMERA_TEST_DEPS=(
	v4l-utils
)

libcamera_install_pkgs() {
	echo "Installing test dependencies"

	apt update
	apt install -y ${PKGS_LIBCAMERA_TEST_DEPS[@]}
}

libcamera_unit_test() {
	echo "Running libcamera tests in a qemu VM"

	virtme-ng --verbose --skip-modules --force-9p \
		--rwdir "$PWD/build" \
		--run /opt/linux/bzImage \
		--exec "meson test -C build --no-rebuild --print-errorlogs; echo \\\$? > build/.test-status"

	local status=$(cat build/.test-status)
	echo "Test result exit state: $status"
	rm build/.test-status

	if [[ $status != 0 ]] ; then
		exit $status
	fi
}

run libcamera_install_pkgs
run libcamera_unit_test
