#!/bin/bash

# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: © 2023 Laurent Pinchart <laurent.pinchart@ideasonboard.com>
#
# Build libcamera

set -e

source "$(dirname "$0")/lib.sh"
source "$(dirname "$0")/build-libcamera-common.sh"

libcamera_build() {
	echo "Building libcamera"

	meson compile -C build -j$BUILD_CONCURRENT_JOBS
}

run libcamera_install_pkgs
run libcamera_setup
run libcamera_build collapsed
